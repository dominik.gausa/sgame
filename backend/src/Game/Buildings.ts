import Planet from './Planet'
import Ressources, {ResTypes} from './Ressources';
import * as DEBUG from 'debug';
import BUILDINGS from './Buildings/BuildingList';
import UpgradeReqType from './IUpgradeReq';

const LOG = DEBUG.default('Buildings');






class Buildings {
  buildingsLvl: {[param: string]: number};
  planet: Planet | null;
  
  constructor(buildingsLvl: {[param: string]: number} = {}) {
    Object.keys(BUILDINGS).forEach(building => {
      if(!(building in buildingsLvl) || !buildingsLvl[building]) {
        buildingsLvl[building] = 0;
      }
    });
    this.buildingsLvl = buildingsLvl;
    this.planet = null;
  }

  SetPlanet(planet: Planet): void {
    this.planet = planet;
  }

  GetLevel(building: string): number {
    return this.buildingsLvl[building] || 0;
  }

  UpgradeReqs(building: string): UpgradeReqType {
    let nextLevel = this.planet!.buildings.GetLevel(building);
    let upgradeReqs = BUILDINGS[building].UpgradeReqs(nextLevel);
    if(upgradeReqs.ressources) 
      Object.entries(upgradeReqs.ressources).forEach(e => {
        (<any>upgradeReqs.ressources)[e[0]] = e[1].toFixed(2);
      });
    return upgradeReqs;
  }

  CanUpgrade(building: string): boolean {
    if(!this.planet) throw('Planet null');
    if(!(building in BUILDINGS)) return false;

    let nextLevel = this.planet.buildings.GetLevel(building);
    let upgradeReqs = BUILDINGS[building].UpgradeReqs(nextLevel);
    let res = new Ressources(upgradeReqs.ressources);

    if(!this.Atleast(upgradeReqs.buildings))
      return false;
    
    if(!this.planet.ressources.Atleast(res))
      return false;

    return true;
  }

  Atleast(reqiredlevel: {[param: string] : number} | undefined) {
    if(!reqiredlevel) return true;

    let reqBuildings = true;
    Object.entries(reqiredlevel).forEach(([key, value]) => {
      if(this.planet!.buildings.GetLevel(key) < value)
        reqBuildings = false;
    });

    return reqBuildings;
  }

  Upgrade(building: string): boolean {
    if(!this.planet) throw('Planet null');
    if(!this.CanUpgrade(building)) return false;

    let nextLevel = this.planet.buildings.GetLevel(building);
    let upgradeReqs = BUILDINGS[building].UpgradeReqs(nextLevel);
    this.planet.ressources.Remove(new Ressources(upgradeReqs.ressources));
    this.buildingsLvl[building] += 1;
    this.planet.resGeneration = null;
    Buildings.UpdateBuildings(this.planet);
    return true;
  }

  Stats(): {[param: string]: {lvl: number, resUpgrade: UpgradeReqType, canUpgrade: boolean}} {
    let stats: any = {};
    Object.keys(BUILDINGS).forEach(building => {
      let currLevel = this.planet!.buildings.GetLevel(building);

      stats[building] = {
        lvl: currLevel,
        resUpgrade: this.UpgradeReqs(building),
        canUpgrade: this.CanUpgrade(building)
      };
    });
    return stats;
  }

  static UpdateBuildings(planet: Planet) {
    const energyResources = [
        ResTypes.energyConsumed,
        ResTypes.energyGenerated];
  
    if(!planet.resGeneration) {
      let resGenerated = new Ressources(); 
      Object.keys(BUILDINGS).forEach(building => {
        resGenerated.AddRes(BUILDINGS[building].Generate(planet, 0));
      });

      //Base generation
      resGenerated.Add(ResTypes.energyGenerated, 1);

      let productivityFactor = resGenerated.Get(ResTypes.energyGenerated) / resGenerated.Get(ResTypes.energyConsumed);
      if(productivityFactor > 1) productivityFactor = 1;
      resGenerated.Multiply(productivityFactor, energyResources);

      resGenerated.Add(ResTypes.metal, 5);
      resGenerated.Add(ResTypes.crystal, 3);

      LOG(`Planet ${planet.name} ProdFac: ${productivityFactor} Gets: ${JSON.stringify(resGenerated.JSON())}`);

      planet.resGeneration = resGenerated;
    }

    planet.ressources.AddRes(planet.resGeneration);
    energyResources.forEach(r =>
      planet.ressources.Set(r, planet.resGeneration!.Get(r) | 0));
  }
}

export default Buildings;
