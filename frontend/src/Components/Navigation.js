import React from 'react';
import { Link } from 'react-router-dom';

class Navigation extends React.Component {
  render()
  {
    return (
      <div className="nav-left">
        <ul>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <Link to="/galaxy">Galaxy</Link>
          </li>
          <li>
            <Link to="/planet">planet</Link>
          </li>
          <li>
            <Link to="/account">Account</Link>
          </li>
          <li>
            <Link to="/FleetList">FleetList</Link>
          </li>
        </ul>
      </div>
      );
    }
}
export default Navigation;
