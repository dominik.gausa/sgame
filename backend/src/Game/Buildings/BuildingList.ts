import IBuilding from "./IBuilding";
import MineMetal from "./MineMetal";
import MineCrystal from "./MineCrystal";
import PowerPlant from "./PowerPlant";
import Shipyard from "./ShipYard";
import MineDeut from "./MineDeut";

const BUILDINGS: {[param: string]: IBuilding} = {
  MineMetal: new MineMetal,
  MineCrystal: new MineCrystal,
  MineDeut: new MineDeut,
  PowerPlant: new PowerPlant,
  Shipyard: new Shipyard
};

export default BUILDINGS;
