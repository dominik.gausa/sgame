enum EBuildingType {
  MineMetal = 'MineMetal',
  MineCrystal = 'MineCrystal',
  MineDeut = 'MineDeut',
  PowerPlant = 'PowerPlant',
  Shipyard = 'Shipyard'
};

export default EBuildingType;
