import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import assert from 'assert';
import LoginAs, { LOGINDATA, AuthToken } from "./login";


axios.defaults.baseURL = 'http://localhost:3310';

describe('Authentication', () => {
  it('Acces /login as none', (done) => {
    console.log(axios.defaults.baseURL);
    axios.get('/login').then((res: AxiosResponse<any>) => {
      assert.equal(res.status, 200);
      assert.equal(res.data, "hi");
      done();
    }).catch(e=> done(e));
  });

  it('Acces /api/test as none', (done) => {
    axios.get('/api/test').then((res: AxiosResponse<any>) => {
      done(1);
    }).catch(err => {
      assert.equal(err.response.statusText, "Unauthorized");
      assert.equal(err.response.status, 401);
      done();
    });
  });

  it('Request Token from /login Admin', (done) => {
    LoginAs(LOGINDATA.admin)
        .then((res: AxiosResponse<any>) => {
      assert.equal(res.status, 200);
      assert.equal(res.data.login, true);
      assert.notEqual(res.data.token, undefined);
      assert.equal(typeof(res.data.token), 'string');
      done();
    }).catch(done);
  });

  it('Request Token from /login Incorrect', (done) => {
    LoginAs({user: 'admin', pass: '123'})
        .then((res: AxiosResponse<any>) => {
      assert.equal(res.status, 200);
      assert.equal(res.data.login, false);
      assert.equal(res.data.token, undefined);

      done();
    }).catch(e=> done(e));
  });

  it('Acces /api/test as Admin', (done) => {
    LoginAs(LOGINDATA.admin)
        .then((res: AxiosResponse<any>) => {

      axios.get('/api/test', {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        assert.equal(res!.data.user, 'admin');
        assert.equal(res!.data.id, 1);
        done();
      }).catch(done);
    }).catch(done);
  });
});
