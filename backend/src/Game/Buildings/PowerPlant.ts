import Planet from "../Planet";
import IBuilding from "./IBuilding";
import Ressources, { ResTypes } from "../Ressources";
import EBuildingType from "./EBuildingType";
import UpgradeReqType from "../IUpgradeReq";


export default class PowerPlant implements IBuilding {
  UpgradeReqs(level: number): UpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 120 * Math.pow(1.1, level),
        [ResTypes.crystal]: 90 * Math.pow(1.1, level),
      }
    };
  }

  Generate(planet: Planet, levelOffset: number = 0): Ressources {
    let level = planet.buildings.GetLevel(EBuildingType.PowerPlant);
    level += levelOffset;
    return new Ressources({
      [ResTypes.energyGenerated]: 2 * Math.pow(1.05, level)
    });
  }
};
