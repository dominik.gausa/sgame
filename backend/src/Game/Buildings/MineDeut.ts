import Planet from "../Planet";
import IBuilding from "./IBuilding";
import Ressources, { ResTypes } from "../Ressources";
import EBuildingType from "./EBuildingType";
import UpgradeReqType from "../IUpgradeReq";

export default class MineDeut implements IBuilding {
  UpgradeReqs(level: number): UpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 120 * Math.pow(1.2, level),
        [ResTypes.crystal]: 80 * Math.pow(1.2, level),
      },
      buildings: {
        [EBuildingType.MineMetal]: 2,
        [EBuildingType.MineCrystal]: 2,
        [EBuildingType.PowerPlant]: 2
      }
    };
  }

  Generate(planet: Planet, levelOffset: number = 0): Ressources {
    let level = planet.buildings.GetLevel(EBuildingType.MineDeut);
    level += levelOffset;
    return new Ressources({
      [ResTypes.deuterium]:           1 * Math.pow(1.05, level),
      [ResTypes.energyConsumed]:   0.75 * Math.pow(1.05, level)
    });
  }
}