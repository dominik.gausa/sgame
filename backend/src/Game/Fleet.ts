import ShipTypes, {EShips} from './Ships/ShipList';
import Planet from './Planet';
import * as DEBUG from 'debug';

const LOG = DEBUG.default('Fleet');
const LOGV = LOG.extend('Verbose');

let FLEETS: Array<Fleet> = [];

enum EDirection {
  TO_HOME, TO_TARGET
};

class Fleet
{
  ships: {[param: string]: number}
  target: Planet | null;
  started: number;
  home: Planet;
  timeOfArrival: number;
  direction: EDirection;

  constructor(home: Planet, ships: {[param: string]: number} = {}){
    this.home = home;
    this.started = 0;
    this.target = null;
    this.timeOfArrival = -1;
    this.direction = EDirection.TO_HOME;

    this.ships = {};
    Fleet.GetTypes().forEach(t => {
      if(ships[t] == undefined) {
        this.ships[t] = 0;
      }else{
        this.ships[t] = ships[t];
      }
    });
    LOGV(`New ${JSON.stringify(this.JSON())}`);

    FLEETS.push(this);
  }

  Attack(planet: Planet): boolean {
    LOGV(`Attack ${this.home.GetPosShort()} > ${this.target!.GetPosShort()}`);

    if(this.target || this.started) return false;

    this.target = planet;
    
    this.started = Date.now();
    this.direction = EDirection.TO_TARGET;

    this.timeOfArrival = this.started + this.GetFlightTimeTo(planet);
    LOGV(`Flighttime: ${this.timeOfArrival - this.started}`);
    return true;
  }

  GetFlightTimeTo(planet: Planet): number {
    let flightTime = 0;
    flightTime += 1 * Math.abs(this.home.position.idx - planet.position.idx);
    flightTime += 4 * Math.abs(this.home.position.solar - planet.position.solar);
    flightTime += 10 * Math.abs(this.home.position.galaxy - planet.position.galaxy);
    return flightTime * 1000;
  }

  Append(ships: {[param: string]: number}) {
    LOGV(`Append to Fleet on ${this.home.GetPosShort()}`);
    LOGV(`Appending ships: ${JSON.stringify(ships)}`);
    Fleet.GetTypes().forEach(t => {
      if(t in ships) {
        this.ships[t] += ships[t];
      }
    });
  }

  Join(fleet: Fleet) {
    LOGV(`Join to Fleet on ${this.home.GetPosShort()}`);
    LOGV(`Appending ships: ${JSON.stringify(this.home.fleet.ships)}`);
    Fleet.GetTypes().forEach(t => {
      LOGV(`- ${t}    ${this.ships[t]}   ${fleet.ships[t]}`);
      this.ships[t] += fleet.ships[t];
    });
    fleet.RemoveFromList();
  }

  RemoveFromList() {
    LOGV(`Remove from List Fleet ${JSON.stringify(this.home.position.JSON())}`);
    let index = FLEETS.indexOf(this);
    if(index >= 0) {
      FLEETS.splice(index, 1);
    }
  }

  FormFleet(ships: {[param: string]: number}): Fleet | null {
    LOGV(`Try to form Fleet on ${this.home.GetPosShort()}`);
    LOGV(`Ships available: ${JSON.stringify(this.ships)}`);
    LOGV(`Ships requested: ${JSON.stringify(ships)}`);

    let enugh = true;
    Fleet.GetTypes().forEach(t => {
      if(!isNaN(ships[t]) && this.ships[t] < ships[t]) {
        enugh = false;
      }
    });

    if(!enugh) return null;
    LOGV(`Having enugh Ships!`);

    let newFleet = new Fleet(this.home);
    Fleet.GetTypes().forEach(t => {
      if(!isNaN(ships[t]))
        this.ships[t] -= ships[t];
    });

    newFleet.Append(ships);
    return newFleet;
  }

  static GetTypes(): Array<string> {
    let types: Array<string> = [];

    Object.entries(EShips).forEach(v => {
      types.push(v[1]);
    })

    return types;
  }

  Update() {
    if(this.timeOfArrival > 0 
          && this.timeOfArrival < Date.now()) {
      switch(this.direction) {
        case EDirection.TO_TARGET:
          LOGV(`Fight: ${JSON.stringify(this.home.position)} > ${JSON.stringify(this.target!.position)}`);
          this.Fight(this.target!.fleet);
          this.started = Date.now();
          this.direction = EDirection.TO_HOME;
          this.timeOfArrival = this.started + this.GetFlightTimeTo(this.target!);
          break;
        case EDirection.TO_HOME:
          LOGV(`Fleet is back on: ${JSON.stringify(this.home.position)}`);
          this.target = null;
          this.home.fleet.Join(this);
          break;
      }
    } 
  }

  Fight(f2: Fleet): boolean {

    return false;
  }

  JSON(): any {
    return {
      home: this.home.position.JSON(),
      timeOfArrival: this.timeOfArrival,
      direction: this.direction,
      target: !this.target ? undefined : this.target.position.JSON(),
      ships: this.ships
    };
  }

  static GetFromPlayer(playerId: number): Array<Fleet> {
    LOGV(`Get all Fleets for Player ${playerId}`);
    let fleets = FLEETS.filter(f => f.home.playerId == playerId);
    LOGV(`Found total of ${fleets.length} fleets`);
    return fleets;
  }

  static UpdateAll() {
    FLEETS.forEach(fleet => fleet.Update());
  }
}

export default Fleet;
