import IShip, {ShipStats, EShipStats} from './IShip'
import Ressources, {ResTypes} from '../Ressources'
import EBuildingType from '../Buildings/EBuildingType';
import UpgradeReqType from '../IUpgradeReq';

export default class SolarSat implements IShip {
  stats: ShipStats;
  
  constructor() {
    this.stats = new ShipStats({
      [EShipStats.Armor]: 1000,
      [EShipStats.Shield]: 10,
      [EShipStats.Damage]: 0
    });
  }

  UpgradeReqs(level:number = 0): UpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 30,
        [ResTypes.crystal]: 10,
      }, buildings: {
        [EBuildingType.Shipyard]: 1
      }
    };
  }

  GetName(): string {
    return 'SolarSat';
  }

  Stats(): ShipStats {
    return this.stats;
  }
  
}