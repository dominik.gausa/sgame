import Planet from "./Planet";

class Player {
  playerId: number;
  name: string;
  planets: Array<Planet>;

  constructor(playerId: number, name: string, planets: Array<Planet>){
    this.playerId = playerId;
    this.name = name;
    this.planets = planets;
  }
}

export default Player;
