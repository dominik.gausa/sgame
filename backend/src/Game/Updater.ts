import Planet from "./Planet";
import Fleet from "./Fleet";
import * as DEBUG from 'debug';

const LOG = DEBUG.default('Update');
const LOGV = DEBUG.default('Verbose');

function UpdaterStart() {
  let iteration = 0;
  setInterval(() => {
    LOGV(`Update ${iteration++}`);
    Planet.UpdateAll();
    Fleet.UpdateAll();
  }, 1000);
}

export default UpdaterStart;
