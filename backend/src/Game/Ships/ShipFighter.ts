import IShip, {ShipStats, EShipStats} from './IShip'
import Ressources, {ResTypes} from '../Ressources'
import Buildings from '../Buildings';
import EBuildingType from '../Buildings/EBuildingType';
import UpgradeReqType from '../IUpgradeReq';

export default class ShipFighter implements IShip {
  stats: ShipStats;
  
  constructor() {
    this.stats = new ShipStats({
      [EShipStats.Armor]: 1000,
      [EShipStats.Shield]: 10,
      [EShipStats.Damage]: 100
    });
  }

  UpgradeReqs(level:number = 0): UpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 30,
        [ResTypes.crystal]: 10,
      }, buildings: {
        [EBuildingType.Shipyard]: 2
      }
    };
  }

  GetName(): string {
    return 'Fighter';
  }

  Stats(): ShipStats {
    return this.stats;
  }
  
}