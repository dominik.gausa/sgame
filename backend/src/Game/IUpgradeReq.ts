interface UpgradeReqType  
{
  ressources?: {[param: string]: number} | undefined, 
  buildings?: {[param: string]: number}, 
};
export default UpgradeReqType;

export interface IUpgradeReq {
  UpgradeReqs(level:number): UpgradeReqType;
}


