import Ressouces from '../Ressources';
import { IUpgradeReq } from '../IUpgradeReq';

export enum EShipStats {
  Armor = 'Armor',
  Shield = 'Shield',
  Damage = 'Damage'
};

export class ShipStats {
  stats: {[param: string]: number};

  constructor(stats: {[param: string]: number} = {}) {
    this.stats = stats;
  }
}

export default interface IShip extends IUpgradeReq {
  GetName(): string;
  Stats(): ShipStats;
}
