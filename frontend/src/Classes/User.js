import * as event from 'events';
import Com from './Com';

class UserData
{
  constructor()
  {
    this.token = localStorage.getItem('token');
    this.name = 'unknown';
    this.event = new event.EventEmitter();
    this.event.on('update', token => {
      console.log('Updated ' + token);
    });
    this.event.emit('update');
    this.planetsUpdated = Date.now();

    this.event.on('selectPlanet', () => {
      console.log('selectPlanet', this.GetPlanetSelected().name);
    });
  }

  GetData(){
    var base64Url = this.token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  UpdateFromToken(token){
    this.event.emit('update', token);
    localStorage.setItem('token', token);
    this.token = token;
  }

  GetName() {
    return this.name;
  }

  GetToken() {
    return this.token;
  }

  SetPlanets(planets) {
    this.planetsUpdated = Date.now();
    this.planets = planets;
    this.event.emit('selectPlanet');
    this.event.emit('update');
  }

  UpdatePlanets() {
    Com.Get(`/api/planet/my`).then(res => {
      this.SetPlanets(res.data);
    }).catch();
  }

  GetPlanets() {
    return this.planets;
  }

  SelectPlanet(planet) {
    this.planetSelected = this.planets.indexOf(planet);
    this.event.emit('selectPlanet');
  }

  GetPlanetSelected() {
    if(this.planetSelected)
      return this.planets[this.planetSelected];
    if(this.planets && this.planets.length > 0)
      return this.planets[0];
    return null;
  }

  /**
   * Estimate Ressources of selected Planet
   */
  EstimateResOfSelPlanet() {
    let gen = {
      metal: 0,
      crystal: 0,
      deuterium: 0
    };
    let res = {
      metal: 0,
      crystal: 0,
      deuterium: 0,
      efficiency: 0
    };

    let planet = userData.GetPlanetSelected();
    if(planet) {
      let diff = Date.now() - userData.planetsUpdated;
      diff /= 1000;

      if(planet.ressources) res = planet.ressources;
      if(planet.resGeneration) gen = planet.resGeneration;
      
      res = {
        metal:      (res.metal + diff * gen.metal).toFixed(2),
        crystal:    (res.crystal + diff * gen.crystal).toFixed(2),
        deuterium:  (res.deuterium + diff * gen.deuterium).toFixed(2),
        efficiency: Math.min(1, res.energyGenerated / res.energyConsumed)
      }
    }

    return {gen, res};
  }
};

const userData = new UserData();

export { userData };
export default UserData;
