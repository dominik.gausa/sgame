import express = require('express');
import jwt = require('jsonwebtoken');
import * as cors from "cors";
import * as helmet from "helmet";
import * as bodyParser from "body-parser";
import DB from './DB';
import Config from './Config'
import * as Game from './Game/Game'

const DEBUG = require('debug');
const LOG = DEBUG.default('Server');

LOG('Hey ho!');

(new DB()).Init();

const app: express.Application = express();

app.use(cors.default());
app.use(helmet.default());
app.use(bodyParser.json());
app.get('/', (req, res) => {
  res.send('Hello World!');
});


app.post('/login', async (req, res) => {
  console.log(req.body);
  const user : string = req.body.user;
  const pass : string = req.body.pass;

  const userRow: any = await DB.Get().Get("SELECT * FROM user WHERE name=?", [user]);
  if(userRow.pass === pass) {
    let token = jwt.sign({
        user,
        id: userRow.num,
        access: 'all'},
        Config.JWT_SECRET());
    res.json({
      login: true,
      token: token
    });
  }else{
    res.json({
      login: false
    });
  }
});



app.use('/api', Game.default);

app.use((req, res) => {
  res.end('hi');
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  LOG(`App is listening on port ${PORT}!`);
});
