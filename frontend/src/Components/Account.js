import React from 'react';
import Com from '../Classes/Com';


class Account extends React.Component {
  state = {
    content: ''
  }
  
  constructor(props) {
    super(props);
    this.Update = this.Update.bind(this);
  }

  componentDidMount() {
    this.Update();
  }

  Update(){
    Com.Get('/api/test').then(result => {
      this.setState({content: result.data});
    });
  }

  render() {
    return (
      <div className="content">
        overview<br />
        <a href={this.Update}>Update</a>
        <hr />
        {JSON.stringify(this.state.content)}
      </div>
    );
  }
}

export default Account;
