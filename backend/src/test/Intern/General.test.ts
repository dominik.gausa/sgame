import assert from 'assert';


describe('General', () => {
  it('Enum iteration', () => {
    enum EEnum {
      One = 'one',
      Two = 'two',
      Three = 'three',
    };
    let types: Array<string> = [];
    Object.entries(EEnum).forEach(v => {
      types.push(v[1]);
    })

    assert.deepEqual(types, ['one', 'two', 'three']);
  });

  it('Date', () => {
    let start: number = Date.UTC(2020, 0, 1, 0, 0, 0);
    console.debug({start});
  });
});
