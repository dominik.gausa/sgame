
export enum ResTypes {
  energyGenerated = 'energyGenerated',
  energyConsumed = 'energyConsumed',
  metal = 'metal',
  crystal = 'crystal',
  deuterium = 'deuterium'
}

class Ressources {
  ressources: {[param: string]: number};

  constructor(ressources: {[param: string]: number} = {}) {
    let res = Object.keys(ressources);
    let unknown = res.find(e => 
        !(e in Ressources.GetTypes())
      );
    //TODO  throw 'Init with unknown ressource';

    Ressources.GetTypes().forEach(t => {
      if(!ressources[t]) ressources[t] = 0;
    });
    this.ressources = ressources;
  }

  static GetTypes(): Array<string> {
    let types: Array<string> = [];
    
    Object.entries(ResTypes).forEach(v => {
      types.push(v[1]);
    })

    return types;
  }

  Get(res: string): number {
    if(res in Ressources.GetTypes())
      throw 'Requested unknown Ressource';
    return this.ressources[res];
  }

  Set(res: string, amount:number): void {
    if(res in Ressources.GetTypes())
      throw 'Requested unknown Ressource';

    this.ressources[res] = amount;
  }

  Add(res: string, amount:number): void {
    if(res in Ressources.GetTypes())
      throw 'Requested unknown Ressource';

    this.Set(res, this.Get(res) + amount);
  }

  AddRes(res: Ressources, except: Array<ResTypes> = []): boolean {
    let more: boolean = true;
  
    Object.keys(this.ressources).forEach(r => {
      if(except.indexOf(<ResTypes>r) < 0)
        this.Add(r, res.Get(r));
    });
    return more;
  }

  Multiply(factor: number, except: Array<ResTypes> = []): void {
    Object.keys(this.ressources).forEach(r => {
      if(except.indexOf(<ResTypes>r) < 0)
        this.Set(r, this.Get(r) * factor);
    });
  }

  Remove(res: Ressources): boolean {
    let more: boolean = true;
    if(!this.Atleast(res)) return false;
  
    Ressources.GetTypes().forEach(r => 
      this.Add(r, -res.Get(r))
    );
    return more;
  }

  Atleast(res: Ressources): boolean {
    let more: boolean = true;
    Ressources.GetTypes().forEach(r => {
      if(this.Get(r) < res.Get(r))
        more = false;
    });
    return more;
  }

  JSON(): any {
    let ressources: {[param: string]: number} = {};
    Object.entries(this.ressources).forEach(e => {
      ressources[e[0]] = +e[1].toFixed(2);
    });
    return ressources;
  }
}

export default Ressources;
