import Planet from "../Planet";
import IBuilding from "./IBuilding";
import Ressources, { ResTypes } from "../Ressources";
import EBuildingType from "./EBuildingType";
import ShipTypes, { EShips } from "../Ships/ShipList";
import UpgradeReqType from "../IUpgradeReq";


export default class Shipyard implements IBuilding {
  UpgradeReqs(level: number): UpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 120 * Math.pow(1.1, level),
        [ResTypes.crystal]: 90 * Math.pow(1.1, level),
      },
      buildings: {
        [EBuildingType.MineMetal]: 4,
        [EBuildingType.MineCrystal]: 4,
        [EBuildingType.MineDeut]: 2,
        [EBuildingType.PowerPlant]: 2
      }
    };
  }

  UpgradeCost(planet: Planet, levelOffset: number = 0): Ressources {
    let currLevel = planet.buildings.GetLevel(EBuildingType.Shipyard);
    currLevel += levelOffset;
    return new Ressources({
      [ResTypes.metal]: 120 * Math.pow(1.1, currLevel),
      [ResTypes.crystal]: 90 * Math.pow(1.1, currLevel),
    });
  }

  Generate(planet: Planet, levelOffset: number = 0): Ressources {
    let level = planet.buildings.GetLevel(EBuildingType.Shipyard);
    level += levelOffset;
    return new Ressources({
      [ResTypes.energyGenerated]: planet.fleet.ships[EShips.SolarSat] * 0.1
    });
  }
};
