import Planet from "../Planet";
import IBuilding from "./IBuilding";
import Ressources, { ResTypes } from "../Ressources";
import EBuildingType from "./EBuildingType";
import UpgradeReqType from "../IUpgradeReq";


export default class MineCrystal implements IBuilding {
  UpgradeReqs(level: number): UpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 100 * Math.pow(1.2, level),
        [ResTypes.crystal]: 70 * Math.pow(1.2, level)
      }
    };
  }

  Generate(planet: Planet, levelOffset: number = 0): Ressources {
    let level = planet.buildings.GetLevel(EBuildingType.MineCrystal);
    level += levelOffset;
    return new Ressources({
      [ResTypes.crystal]:         3 * Math.pow(1.05, level),
      [ResTypes.energyConsumed]:  0.75 * Math.pow(1.05, level)
    });
  }
};
