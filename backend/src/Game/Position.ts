class Position {
  galaxy: number;
  solar: number;
  idx: number;

  constructor(galaxy: number,
              solar: number,
              idx: number) {
    this.galaxy = galaxy;
    this.solar = solar;
    this.idx = idx;
  }

  Set(galaxy: number,
      solar: number,
      idx: number) {
    this.galaxy = galaxy;
    this.idx = idx;
  }

  JSON(): any {
    return {
      galaxy: this.galaxy,
      solar: this.solar,
      idx: this.idx
    };
  }
}

export default Position;
