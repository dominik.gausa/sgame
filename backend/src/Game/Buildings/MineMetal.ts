import Planet from "../Planet";
import IBuilding from "./IBuilding";
import Ressources, { ResTypes } from "../Ressources";
import EBuildingType from "./EBuildingType";
import UpgradeReqType from "../IUpgradeReq";

export default class MineMetal implements IBuilding {
  UpgradeReqs(level: number): UpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 120 * Math.pow(1.2, level),
        [ResTypes.crystal]: 30 * Math.pow(1.2, level),
      }
    };
  }

  Generate(planet: Planet, levelOffset: number = 0): Ressources {
    let level = planet.buildings.GetLevel(EBuildingType.MineMetal);
    level += levelOffset;
    return new Ressources({
      [ResTypes.metal]:           5 * Math.pow(1.05, level),
      [ResTypes.energyConsumed]:  1 * Math.pow(1.05, level)
    });
  }
}