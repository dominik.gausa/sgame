import express = require('express');
import expressjwt = require('express-jwt');
import Planet from './Planet'
import Config from '../Config';
import * as DEBUG from 'debug';
import Fleet from './Fleet';
import UpdaterStart from './Updater';
import DummyPlanets from './DummyPlanets';
import Validator from './Validator';

const LOG = DEBUG.default('API');
const LOGV = LOG.extend('Verbose');

const router: express.Router = express.Router();

router.use(expressjwt({secret: Config.JWT_SECRET(), requestProperty: 'auth'}));
router.use((err: { name: string; }, req: any, res: any, next: any) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('invalid token...');
  }
});

router.use((req: any, res: any, next: any) => {
  LOGV(`user: ${req.auth.id}   ${req.url}`);
  next();
});

router.get('/test', (req: any, res: any, next: any) => {
  res.json((<any>req)['auth']);
});






router.get('/galaxy', (req: any, res: any, next: any) => {
  let planets = Planet.GetAll().flat().flat();
  res.json(planets.map(p => p.jsonShort()));
});

router.get('/galaxy/:gal/:sol', (req: any, res: any, next: any) => {
  let schema = {
    gal: Validator.IsNumber,
    sol: Validator.IsNumber
  };

  if(!Validator.validate(schema, req.params)) {
    res.sendStatus(400);
    return;
  }

  let sol: Array<Planet> = [];
  sol = Planet.GetSolsys(req.params.gal, req.params.sol);

  res.json(sol.map(p => p.jsonShort()));
});

router.get('/planet/show/:gal/:sol/:idx', (req: any, res: any, next: any) => {
  let schema = {
    gal: Validator.IsNumber,
    sol: Validator.IsNumber,
    idx: Validator.IsNumber
  };

  if(!Validator.validate(schema, req.params)) {
    res.sendStatus(400);
    return;
  }

  let planet: Planet | null = null;

  planet = Planet.GetPlanet(req.params.galaxy, req.params.sol, req.params.idx);

  if(!planet){
    res.sendStatus(400);
    return;
  }

  res.json(planet.JSON());
});

router.get('/planet/my', (req: any, res: any, next: any) => {
  let planet: Array<Planet> = [];

  planet = Planet.GetPlanetsOfPlayer(req.auth.id);

  if(!planet){
    res.sendStatus(400);
    return;
  }

  res.json(planet.map(p => p.JSON()));
});


router.get('/planet/building/up/:gal/:sol/:idx/:building', (req: any, res: any, next: any) => {
  let schema = {
    gal: Validator.IsNumber,
    sol: Validator.IsNumber,
    idx: Validator.IsNumber,
    building: Validator.IsSet,
  };

  if(!Validator.validate(schema, req.params)) {
    res.sendStatus(400);
    return;
  }
  
  let planet: Planet | null = null;

  LOGV(`/planet/building/up  Params OK`);
  planet = Planet.GetPlanet(req.params.gal, req.params.sol, req.params.idx);

  if(!planet){
    LOGV(`/planet/building/up  !Planet`);
    res.sendStatus(400);
    return;
  }

  if(planet.playerId != req.auth.id) {
    LOGV(`/planet/building/up  Planet not owned by`);
    res.json({error: 'Not your Planet'});
  }else{
    res.json({
      result: planet.buildings.Upgrade(req.params.building),
      newLvl: planet.buildings.GetLevel(req.params.building),
    });
  }
});

router.get('/fleet/list', (req: any, res: any, next: any) => {
  let fleets = Fleet.GetFromPlayer(req.auth.id);
  res.json(fleets.map(f => f.JSON()));
});

router.post('/fleet/:command/:gal/:sol/:idx/:tgal/:tsol/:tidx', (req: any, res: any, next: any) => {
  let schema = {
    command: Validator.IsAny(['attack']),
    gal: Validator.IsNumber,
    sol: Validator.IsNumber,
    idx: Validator.IsNumber,
    tgal: Validator.IsNumber,
    tsol: Validator.IsNumber,
    tidx: Validator.IsNumber
  };

  LOGV(`Req Body: ${JSON.stringify(req.body)}`);

  if(!Validator.validate(schema, req.params)) {
    res.sendStatus(400);
    return;
  }

  let planetSrc = Planet.GetPlanet(req.params.gal, req.params.sol, req.params.idx);
  let planetDst = Planet.GetPlanet(req.params.tgal, req.params.tsol, req.params.tidx);
  if(!planetSrc
        || planetSrc.playerId != req.auth.id
        || !planetDst) {
    res.sendStatus(400);
    return;
  }
  LOGV(`Planets OK`);

  let fleet = planetSrc.fleet.FormFleet(req.body);
  if(!fleet) {
    res.sendStatus(400);
    return;
  }
  LOGV(`Created Fleet: ${JSON.stringify(fleet.JSON())}`);

  fleet.Attack(planetDst);
  res.json(fleet.JSON());
});

router.get('/planet/my', (req: any, res: any, next: any) => {
  let planet: Array<Planet> = [];

  planet = Planet.GetPlanetsOfPlayer(req.auth.id);

  if(!planet){
    res.sendStatus(400);
    return;
  }

  res.json(planet.map(p => p.JSON()));
});

UpdaterStart();
DummyPlanets();

export default router;
