import Planet from "./Planet";
import Position from "./Position";
import Ressources from "./Ressources";
import Buildings from "./Buildings";
import EBuildingType from "./Buildings/EBuildingType";
import Bot from "./Bot";
import Fleet from "./Fleet";
import { EShips } from "./Ships/ShipList";

function DummyPlanets() {
  let planet = new Planet(new Position(0, 1, 0), 1, 'Colony', new Ressources(), new Buildings({
    MineMetal: 5,
    MineCrystal: 3
  }));
  
  planet .fleet.Join(
    new Fleet(planet, {
      [EShips.Fighter]: 100,
      [EShips.SolarSat]: 10
    }));

  new Planet(new Position(0, 1, 3), 1, 'Colony', new Ressources(), new Buildings({
    MineMetal: 5,
    MineCrystal: 3
  }));

  let bot = new Bot(2, "Bot1",
    [
      new Planet(new Position(0, 2, 3), 2, 'Bot1:1', new Ressources(), new Buildings({
        MineMetal: 5,
        MineCrystal: 3
      })),
      new Planet(new Position(0, 2, 5), 3, 'Bot1:2', new Ressources(), 
        new Buildings({
          MineMetal: 5,
          MineCrystal: 3
        })
      ),
    ]
  );

  bot.planets[1].fleet.Join(
    new Fleet(bot.planets[1], {
      [EShips.SolarSat]: 10
    }));

    
  new Planet(new Position(1, 2, 2), 1, 'Colony2', new Ressources(), new Buildings({
    MineMetal: 3,
    MineCrystal: 3
  }));

  new Planet(new Position(1, 6, 7), 2, 'Colony3', new Ressources(), new Buildings({
    MineMetal: 3,
    MineCrystal: 3
  }));

  Bot.Start();
}

export default DummyPlanets;
